
let pokemonTypes = {};
let trainers = {};
let tableParentId = 'table';
let detailsParentId = 'details';

function getRowId(resource) {
    return `${resource.id}_row`
}

function updateDetails(pokemonTypeId) {
    const pt = pokemonTypes?.data?.find(item => item.id === pokemonTypeId);
    const parent = document.getElementById(detailsParentId);

    parent.innerHTML = `
    <div class="card" id="${pt.id}_card">
      <img src="${pt.imgUrl}" class="card-img-top" alt="${pt.classification}">
      <div class="card-body">
        <h5 class="card-title">${pt.name} #${pt.pokedexNumber}</h5>
        <p class="card-text">
            Japanese name: ${pt.japaneseName} </br>
            Classification: ${pt.classification} </br>
            Abilities: ${pt.abilities?.join(", ") || "none"} </br>
            Type: ${pt.primaryType}${pt.secondaryType ? " , " + pt.secondaryType : ""}
        </p>
      </div>
    </div>
    `
}

function pokemonTypeToRow(pokemonType) {
    return `
    <tr id="${getRowId(pokemonType)}" onclick="updateDetails('${pokemonType?.id?.trim()}')">
        <th scope="row">${pokemonType.id}</th>
        <td>${pokemonType.name}</td>
        <td>#${pokemonType.pokedexNumber}</td>
        <td>${pokemonType.primaryType}</td>
        <td>${pokemonType.secondaryType || '-'}</td>
    </tr>
    `
}

function createPokemonTypesTable() {
    const tableParent = document.getElementById(tableParentId);
    tableParent.innerHTML = `
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Pokedex Number</th>
                    <th scope="col">Primary Type</th>
                    <th scope="col">Secondary Type</th>
                </tr>
            </thead>
            <tbody>
                ${
                    pokemonTypes.data.map(resource => `${pokemonTypeToRow(resource)}`).join("\n")
                    || "no data"
                }
            </tbody>
        </table>
    `
}

function trainersToRow(trainer) {
    return `
    <tr id="${getRowId(trainer)}" onclick="updateTrainerDetails('${trainer?.id?.trim()}')">
        <th scope="row">${trainer.id}</th>
        <td>${trainer.name}</td>
    </tr>
    `
}

function createTrainersTable() {
    const tableParent = document.getElementById(tableParentId);
    tableParent.innerHTML = `
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                </tr>
            </thead>
            <tbody>
                ${
        trainers.data.map(resource => `${trainersToRow(resource)}`).join("\n")
        || "no data"
    }
            </tbody>
        </table>
        <div id="pagination"></div> 

    `
}

function updateTrainerDetails(trainerID) {
    const pt = trainers?.data?.find(item => item.id === trainerID);
    const parent = document.getElementById(detailsParentId);
    parent.innerHTML = `
    <div class="card" id="${pt.id}_card">
      <img src="${pt.profileUrl}" class="card-img-top" alt="${pt.classification}">
      <div class="card-body">
        <h5 class="card-title">${pt.name}</h5>
        <p class="card-text">
            Name: ${pt.name} </br>
            ID: ${pt.id} </br>
            Created: ${pt.created} </br>
            Last Updated: ${pt.lastUpDate}
        </p>
      </div>
    </div>
    
    <button onclick = "updateTrainer(${pt.id})" > Update this Trainer </button>
    <button onclick = "deleteTrainer(${pt.id})" > Delete this Trainer </button>
    `
}

function createTrainer() {
    var userinput = prompt("Enter a name to create a new Trainer!")
    localStorage.setItem("userinput", JSON.stringify(userinput));
    fetch ("/pokemon/api/trainers", {
        method:"post",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({name:userinput})
    })
    window.location = window.location.href;
}

function updateTrainer(trainerID){
    var userInput = prompt("Enter the name you want to change this user to.");
    fetch(`/pokemon/api/trainers/${trainerID}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ name: userInput, id: trainerID })
    })
    window.location = window.location.href;
}

function deleteTrainer(trainerID){
    fetch(`/pokemon/api/trainers/${trainerID}`, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json"
        },
    })
    window.location = window.location.href;
}